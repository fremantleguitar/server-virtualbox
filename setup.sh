sudo apt install -y git xclip

mkdir -p $HOME/.ssh
ssh-keygen -f $HOME/.ssh/id_rsa

echo "Paste the clipboard contents into bitbucket allowed keys!"
cat $HOME/.ssh/id_rsa.pub | xclip -sel clip

read -p "Press enter to continue AFTER doing the keys"

git clone git@bitbucket.org:fremantleguitar/invoice-emailer.git

echo "Run this:"

echo "wget -O - https://raw.githubusercontent.com/nhemsley/dotfiles/master/script/install.sh > dotfiles-install.sh"

echo ". ./dotfiles-install.sh"
 